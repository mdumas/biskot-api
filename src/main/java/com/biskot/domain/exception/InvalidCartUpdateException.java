package com.biskot.domain.exception;

import com.biskot.domain.model.ValidationError;

import java.util.List;

public class InvalidCartUpdateException extends RuntimeException {

    private final List<ValidationError> validationErrors;

    public InvalidCartUpdateException(List<ValidationError> validationErrors) {
        this.validationErrors = validationErrors;
    }

    public List<ValidationError> getValidationErrors() {
        return validationErrors;
    }

    @Override
    public String toString() {
        return "InvalidCartUpdateException{" +
                "validationErrors=" + validationErrors +
                '}';
    }

}
