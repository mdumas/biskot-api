package com.biskot.domain.spi;

import com.biskot.domain.model.Cart;

import java.util.Optional;

public interface CartPersistencePort {

    Cart saveCart(Cart cart);

    Optional<Cart> getCart(long id);

}
