package com.biskot.domain.service;

import com.biskot.domain.exception.CartDoesNotExistException;
import com.biskot.domain.exception.InvalidCartUpdateException;
import com.biskot.domain.exception.ProductDoesNotExistException;
import com.biskot.domain.model.*;
import com.biskot.domain.spi.CartPersistencePort;
import com.biskot.domain.spi.ProductPort;
import com.biskot.domain.validator.CartUpdateValidator;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class CartServiceImpl implements CartService {

    private final CartPersistencePort cartPersistencePort;
    private final ProductPort productPort;
    private final CartUpdateValidator cartUpdateValidator;

    public CartServiceImpl(CartPersistencePort cartPersistencePort, ProductPort productPort, CartUpdateValidator cartUpdateValidator) {
        this.cartPersistencePort = cartPersistencePort;
        this.productPort = productPort;
        this.cartUpdateValidator = cartUpdateValidator;
    }

    @Override
    public Cart createCart() {
        return cartPersistencePort.saveCart(new Cart());
    }

    @Override
    public Cart getCart(long cartId) {
        return cartPersistencePort.getCart(cartId).orElse(null);
    }

    @Override
    public void addItemToCart(long cartId, long productId, int quantityToAdd) {
        Cart cart = getCart(cartId);
        if (cart == null) {
            throw new CartDoesNotExistException();
        }

        Product product = productPort.getProduct(productId);
        if (product == null) {
            throw new ProductDoesNotExistException();
        }

        Optional<Item> anyItem = cart.getItems().stream()
                .filter(item -> item.getProductId().equals(productId))
                .findAny();

        if (anyItem.isPresent()) { // Update
            // Should we update product related data if any change ?
            Item item = anyItem.get();
            item.setQuantity(quantityToAdd);
        } else { // Insert
            Item item = createItem(product, quantityToAdd);
            cart.getItems().add(item);
        }

        validate(cart, product, quantityToAdd);

        cartPersistencePort.saveCart(cart);
    }

    private Item createItem(Product product, int quantityToAdd) {
        Item item = new Item();

        item.setProductId(product.getId());
        item.setProductLabel(product.getLabel());
        item.setQuantity(quantityToAdd);
        item.setUnitPrice(product.getUnitPrice());

        return item;
    }

    private void validate(Cart cart, Product product, int quantityToAdd) {
        List<ValidationError> validationErrors = cartUpdateValidator.validate(cart, product, quantityToAdd);
        if (!validationErrors.isEmpty()) {
            throw new InvalidCartUpdateException(validationErrors);
        }
    }


}
