package com.biskot.domain.validator;

import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Product;
import com.biskot.domain.model.ValidationError;
import com.biskot.domain.model.ValidationErrorType;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartUpdateValidator {

    private final Environment environment;

    public CartUpdateValidator(Environment environment) {
        this.environment = environment;
    }

    public List<ValidationError> validate(Cart cart, Product product, int quantityToAdd) {
        List<ValidationError> validationErrors = new ArrayList<>();

        // 1. Added quantity of a product should not exceed the stock availability.
        if (quantityToAdd > product.getQuantityInStock()) {
            validationErrors.add(new ValidationError(ValidationErrorType.NOT_ENOUGH_QUANTITY, product.getQuantityInStock()));
        }

        // 2. Total price of the cart should not exceed 100 euros.
        BigDecimal maxTotalPriceAllowed = environment.getRequiredProperty("business-rules.max-total-price-allowed", BigDecimal.class);
        if (cart.getTotalPrice().compareTo(maxTotalPriceAllowed) > 0) {
            validationErrors.add(new ValidationError(ValidationErrorType.MAX_TOTAL_PRICE_EXCEEDED, maxTotalPriceAllowed));
        }

        // 3. A cart cannot contain more than 3 different products.
        Integer maxProductsAllowed = environment.getRequiredProperty("business-rules.max-products-allowed", Integer.class);
        if (cart.getItems().size() > maxProductsAllowed) {
            validationErrors.add(new ValidationError(ValidationErrorType.MAX_PRODUCTS_EXCEEDED, maxProductsAllowed));
        }

        return validationErrors;
    }

}
