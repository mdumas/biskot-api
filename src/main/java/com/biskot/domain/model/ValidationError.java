package com.biskot.domain.model;

import java.util.Arrays;

public class ValidationError {

    private final ValidationErrorType validationErrorType;
    private final Object[] params;

    public ValidationError(ValidationErrorType validationErrorType, Object... params) {
        this.validationErrorType = validationErrorType;
        this.params = params;
    }

    public ValidationErrorType getValidationErrorType() {
        return validationErrorType;
    }

    public Object[] getParams() {
        return params;
    }

    @Override
    public String toString() {
        return "ValidationError{" +
                "validationErrorType=" + validationErrorType +
                ", params=" + Arrays.toString(params) +
                '}';
    }

}
