package com.biskot.domain.model;

public enum ValidationErrorType {

    NOT_ENOUGH_QUANTITY,
    MAX_TOTAL_PRICE_EXCEEDED,
    MAX_PRODUCTS_EXCEEDED

}
