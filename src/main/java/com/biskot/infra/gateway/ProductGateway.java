package com.biskot.infra.gateway;

import com.biskot.domain.model.Product;
import com.biskot.domain.spi.ProductPort;
import com.biskot.infra.gateway.payload.ProductResponse;
import com.biskot.infra.mapper.ProductMapper;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ProductGateway implements ProductPort {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductGateway.class);

    static final String PRODUCT_CACHE_NAME = "product";

    private final RestTemplate restTemplate;
    private final String productGatewayUrl;
    private final ProductMapper productMapper;
    private final CacheManager cacheManager;

    public ProductGateway(RestTemplateBuilder restTemplateBuilder,
                          @Value("${gateways.product.url}") String productGatewayUrl,
                          ProductMapper productMapper,
                          CacheManager cacheManager)
    {
        this.restTemplate = restTemplateBuilder.errorHandler(new NoOpResponseErrorHandler()).build();
        this.productGatewayUrl = productGatewayUrl;
        this.productMapper = productMapper;
        this.cacheManager = cacheManager;
    }

    @CircuitBreaker(name = "productService", fallbackMethod = "getProductFallback")
    public Product getProduct(long productId) {
        ResponseEntity<ProductResponse> responseEntity = restTemplate.getForEntity(productGatewayUrl + "/products/{id}", ProductResponse.class, productId);
        if (responseEntity.getStatusCode() == HttpStatus.NOT_FOUND) {
            return null;
        }

        if (responseEntity.getStatusCode().isError() || responseEntity.getBody() == null) {
            LOGGER.error("Error trying to fetch product with id {}. Response : {}.", productId, responseEntity);
            throw new IllegalStateException();
        }

        ProductResponse productResponse = responseEntity.getBody();

        addToCache(productResponse);

        return productMapper.map(productResponse);
    }

    // We assume that hitting the cache for a cart service is acceptable. It should not be in a checkout service.
    private Product getProductFallback(long productId, IllegalStateException e) {
        ProductResponse productResponse = getValueFromCache(productId);
        if (productResponse == null) {
            throw e;
        }

        return productMapper.map(productResponse);
    }

    private ProductResponse getValueFromCache(long productId) {
        Cache.ValueWrapper valueWrapper = getCache().get(productId);
        if (valueWrapper != null) {
            return (ProductResponse) valueWrapper.get();
        }

        return null;
    }

    private void addToCache(ProductResponse productResponse) {
        getCache().put(productResponse.getId(), productResponse);
    }

    private Cache getCache() {
        return cacheManager.getCache(PRODUCT_CACHE_NAME);
    }

}
