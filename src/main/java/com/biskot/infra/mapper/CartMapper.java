package com.biskot.infra.mapper;

import com.biskot.domain.model.Cart;
import com.biskot.infra.repository.entity.CartEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartMapper {

    CartEntity map(Cart cart);

    Cart map(CartEntity cartEntity);

}
