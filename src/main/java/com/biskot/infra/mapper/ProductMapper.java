package com.biskot.infra.mapper;

import com.biskot.domain.model.Product;
import com.biskot.infra.gateway.payload.ProductResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product map(ProductResponse productResponse);

}
