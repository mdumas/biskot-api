package com.biskot.infra.repository;

import com.biskot.domain.model.Cart;
import com.biskot.domain.spi.CartPersistencePort;
import com.biskot.infra.mapper.CartMapper;
import com.biskot.infra.repository.entity.CartEntity;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CartRepository implements CartPersistencePort {

    private final JpaCartRepository jpaCartRepository;
    private final CartMapper cartMapper;

    public CartRepository(JpaCartRepository jpaCartRepository, CartMapper cartMapper) {
        this.jpaCartRepository = jpaCartRepository;
        this.cartMapper = cartMapper;
    }

    @Override
    public Cart saveCart(Cart cart) {
        CartEntity cartEntity = cartMapper.map(cart);
        return cartMapper.map(jpaCartRepository.save(cartEntity));
    }

    @Override
    public Optional<Cart> getCart(long id) {
        Optional<CartEntity> optional = jpaCartRepository.findById(id);
        return optional.map(cartMapper::map);

    }

}
