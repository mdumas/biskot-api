package com.biskot.infra.repository;

import com.biskot.infra.repository.entity.CartEntity;
import org.springframework.data.repository.CrudRepository;

public interface JpaCartRepository extends CrudRepository<CartEntity, Long> {}
