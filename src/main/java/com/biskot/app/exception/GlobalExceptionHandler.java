package com.biskot.app.exception;

import com.biskot.app.contract.model.ApiError;
import com.biskot.app.contract.model.Error;
import com.biskot.domain.exception.CartDoesNotExistException;
import com.biskot.domain.exception.InvalidCartUpdateException;
import com.biskot.domain.exception.ProductDoesNotExistException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private final MessageSource messageSource;

    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(CartDoesNotExistException.class)
    public ResponseEntity<ApiError> handleCartDoesNotExistException(HttpServletRequest request, CartDoesNotExistException ex) {
        return handleDoesNotExistException(request, ex, "notFound.cart.message");
    }

    @ExceptionHandler(ProductDoesNotExistException.class)
    public ResponseEntity<ApiError> handleProductDoesNotExistException(HttpServletRequest request, ProductDoesNotExistException ex) {
        return handleDoesNotExistException(request, ex, "notFound.product.message");
    }

    @ExceptionHandler(InvalidCartUpdateException.class)
    public ResponseEntity<ApiError> handleInvalidCartUpdateException(HttpServletRequest request, InvalidCartUpdateException ex) {
        LOGGER.debug("Bad request (business rules) : {}.", request, ex);

        ApiError apiError = createApiError(HttpStatus.BAD_REQUEST, "badRequest.businessRules.message");

        List<Error> errors = ex.getValidationErrors().stream().map(e -> {
            Error error = new Error();
            error.setCode(e.getValidationErrorType().name());
            error.setMessage(resolveMessage("apiError." + e.getValidationErrorType(), e.getParams()));
            return error;
        }).collect(Collectors.toList());

        apiError.setErrors(errors);

        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(apiError);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.debug("Bad request (request format) : {}.", request, ex);

        ApiError apiError = createApiError(HttpStatus.BAD_REQUEST, "badRequest.format.message");

        List<Error> errors = ex.getBindingResult().getFieldErrors().stream().map(e -> {
            Error error = new Error();
            error.setField(e.getField());
            error.setMessage(String.format("%s %s", e.getField(), e.getDefaultMessage()));
            return error;
        }).collect(Collectors.toList());

        apiError.setErrors(errors);

        return ResponseEntity.badRequest()
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(apiError);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.warn("An exception occurred while handling request : {}.", request, ex);

        String message;
        if (status.is4xxClientError()) {
            message = "4XX.default.message";
        } else { // 5XX
            message = "5XX.default.message";
        }

        ApiError apiError = createApiError(status, message);

        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(apiError);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleException(HttpServletRequest request, Exception e) {
        LOGGER.error("Oops, something unexpected happened while handling request : {}.", request, e);
        return createResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, "internalServerError.message");
    }

    private ResponseEntity<ApiError> handleDoesNotExistException(HttpServletRequest request, Exception ex, String messageCode) {
        LOGGER.debug("Resource not found for request : {}.", request, ex);
        return createResponseEntity(HttpStatus.NOT_FOUND, messageCode);
    }

    private ResponseEntity<ApiError> createResponseEntity(HttpStatus status, String messageCode) {
        ApiError apiError = createApiError(status, messageCode);

        return ResponseEntity.status(status)
                .contentType(MediaType.APPLICATION_PROBLEM_JSON)
                .body(apiError);
    }

    private ApiError createApiError(HttpStatus status, String messageCode) {
        ApiError apiError = new ApiError();

        apiError.setStatus(status.value());
        apiError.setTimestamp(OffsetDateTime.now());
        apiError.setMessage(resolveMessage(messageCode));

        return apiError;
    }

    private String resolveMessage(String code, Object... params) {
        return messageSource.getMessage(code, params, LocaleContextHolder.getLocale());
    }

}
