package com.biskot.app.contract.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AddItemRequest
 */

public class AddItemRequest   {
  @JsonProperty("product_id")
  private Long productId;

  @JsonProperty("quantity")
  private Integer quantity;

  public AddItemRequest productId(Long productId) {
    this.productId = productId;
    return this;
  }

  /**
   * The product's unique identifier to add or update
   * minimum: 1
   * @return productId
  */
  @ApiModelProperty(example = "3", required = true, value = "The product's unique identifier to add or update")
  @NotNull

@Min(1L)
  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public AddItemRequest quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * The quantity to add or the updated quantity in case of an already existing product into the cart
   * minimum: 1
   * @return quantity
  */
  @ApiModelProperty(example = "2", required = true, value = "The quantity to add or the updated quantity in case of an already existing product into the cart")
  @NotNull

@Min(1)
  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AddItemRequest addItemRequest = (AddItemRequest) o;
    return Objects.equals(this.productId, addItemRequest.productId) &&
        Objects.equals(this.quantity, addItemRequest.quantity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productId, quantity);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddItemRequest {\n");
    
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

