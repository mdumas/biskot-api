package com.biskot.app.contract.model;

import java.util.Objects;
import com.biskot.app.contract.model.ItemResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CartResponse
 */

public class CartResponse   {
  @JsonProperty("id")
  private Long id;

  @JsonProperty("items")
  @Valid
  private List<ItemResponse> items = null;

  @JsonProperty("total_price")
  private java.math.BigDecimal totalPrice;

  public CartResponse id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * The cart's unique identifier
   * @return id
  */
  @ApiModelProperty(value = "The cart's unique identifier")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public CartResponse items(List<ItemResponse> items) {
    this.items = items;
    return this;
  }

  public CartResponse addItemsItem(ItemResponse itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<>();
    }
    this.items.add(itemsItem);
    return this;
  }

  /**
   * The list of items contained into the cart
   * @return items
  */
  @ApiModelProperty(value = "The list of items contained into the cart")

  @Valid

  public List<ItemResponse> getItems() {
    return items;
  }

  public void setItems(List<ItemResponse> items) {
    this.items = items;
  }

  public CartResponse totalPrice(java.math.BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
    return this;
  }

  /**
   * The total price of the cart
   * @return totalPrice
  */
  @ApiModelProperty(value = "The total price of the cart")

  @Valid

  public java.math.BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(java.math.BigDecimal totalPrice) {
    this.totalPrice = totalPrice;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CartResponse cartResponse = (CartResponse) o;
    return Objects.equals(this.id, cartResponse.id) &&
        Objects.equals(this.items, cartResponse.items) &&
        Objects.equals(this.totalPrice, cartResponse.totalPrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, items, totalPrice);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CartResponse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

