package com.biskot.app.contract.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Error
 */

public class Error   {
  @JsonProperty("code")
  private String code;

  @JsonProperty("field")
  private String field;

  @JsonProperty("message")
  private String message;

  public Error code(String code) {
    this.code = code;
    return this;
  }

  /**
   * The error code in case of business rule violation
   * @return code
  */
  @ApiModelProperty(value = "The error code in case of business rule violation")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Error field(String field) {
    this.field = field;
    return this;
  }

  /**
   * The field name in case of request format error
   * @return field
  */
  @ApiModelProperty(value = "The field name in case of request format error")


  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public Error message(String message) {
    this.message = message;
    return this;
  }

  /**
   * The detailed error message
   * @return message
  */
  @ApiModelProperty(value = "The detailed error message")


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Error error = (Error) o;
    return Objects.equals(this.code, error.code) &&
        Objects.equals(this.field, error.field) &&
        Objects.equals(this.message, error.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, field, message);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Error {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    field: ").append(toIndentedString(field)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

