package com.biskot.app.contract.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ItemResponse
 */

public class ItemResponse   {
  @JsonProperty("product_id")
  private Long productId;

  @JsonProperty("product_label")
  private String productLabel;

  @JsonProperty("quantity")
  private Integer quantity;

  @JsonProperty("unit_price")
  private java.math.BigDecimal unitPrice;

  @JsonProperty("line_price")
  private java.math.BigDecimal linePrice;

  public ItemResponse productId(Long productId) {
    this.productId = productId;
    return this;
  }

  /**
   * The product's unique identifier
   * @return productId
  */
  @ApiModelProperty(value = "The product's unique identifier")


  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public ItemResponse productLabel(String productLabel) {
    this.productLabel = productLabel;
    return this;
  }

  /**
   * The product's label
   * @return productLabel
  */
  @ApiModelProperty(value = "The product's label")


  public String getProductLabel() {
    return productLabel;
  }

  public void setProductLabel(String productLabel) {
    this.productLabel = productLabel;
  }

  public ItemResponse quantity(Integer quantity) {
    this.quantity = quantity;
    return this;
  }

  /**
   * The quantity of this item contained into the cart
   * @return quantity
  */
  @ApiModelProperty(value = "The quantity of this item contained into the cart")


  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public ItemResponse unitPrice(java.math.BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
    return this;
  }

  /**
   * The unit price of the product
   * @return unitPrice
  */
  @ApiModelProperty(value = "The unit price of the product")

  @Valid

  public java.math.BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(java.math.BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public ItemResponse linePrice(java.math.BigDecimal linePrice) {
    this.linePrice = linePrice;
    return this;
  }

  /**
   * The total item price (quantity * unit price)
   * @return linePrice
  */
  @ApiModelProperty(value = "The total item price (quantity * unit price)")

  @Valid

  public java.math.BigDecimal getLinePrice() {
    return linePrice;
  }

  public void setLinePrice(java.math.BigDecimal linePrice) {
    this.linePrice = linePrice;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ItemResponse itemResponse = (ItemResponse) o;
    return Objects.equals(this.productId, itemResponse.productId) &&
        Objects.equals(this.productLabel, itemResponse.productLabel) &&
        Objects.equals(this.quantity, itemResponse.quantity) &&
        Objects.equals(this.unitPrice, itemResponse.unitPrice) &&
        Objects.equals(this.linePrice, itemResponse.linePrice);
  }

  @Override
  public int hashCode() {
    return Objects.hash(productId, productLabel, quantity, unitPrice, linePrice);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ItemResponse {\n");
    
    sb.append("    productId: ").append(toIndentedString(productId)).append("\n");
    sb.append("    productLabel: ").append(toIndentedString(productLabel)).append("\n");
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    unitPrice: ").append(toIndentedString(unitPrice)).append("\n");
    sb.append("    linePrice: ").append(toIndentedString(linePrice)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

