package com.biskot.app.mapper;

import com.biskot.app.contract.model.CartResponse;
import com.biskot.domain.model.Cart;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartResponseMapper {

    CartResponse map(Cart cart);

}
