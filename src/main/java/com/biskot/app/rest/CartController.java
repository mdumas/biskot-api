package com.biskot.app.rest;

import com.biskot.app.contract.api.CartApi;
import com.biskot.app.contract.model.AddItemRequest;
import com.biskot.app.contract.model.CartResponse;
import com.biskot.app.mapper.CartResponseMapper;
import com.biskot.domain.exception.CartDoesNotExistException;
import com.biskot.domain.model.Cart;
import com.biskot.domain.service.CartService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class CartController implements CartApi {

    private final CartService cartService;
    private final CartResponseMapper cartResponseMapper;

    public CartController(CartService cartService, CartResponseMapper cartResponseMapper) {
        this.cartService = cartService;
        this.cartResponseMapper = cartResponseMapper;
    }

    @PostMapping(value = "/carts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CartResponse> createCart() {
        Cart cart = cartService.createCart();

        CartResponse cartResponse = cartResponseMapper.map(cart);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(cartResponse.getId())
                .toUri();

        return ResponseEntity.created(location).body(cartResponse);
    }

    @GetMapping(value = "/carts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CartResponse> getCart(@PathVariable("id") Long cartId) {
        Cart cart = cartService.getCart(cartId);
        if (cart == null) {
            throw new CartDoesNotExistException();
        }

        return ResponseEntity.ok(cartResponseMapper.map(cart));
    }

    @PutMapping(value = "/carts/{id}/items", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addItemToCart(@PathVariable("id") Long cartId, @Valid AddItemRequest addItemRequest) {
        cartService.addItemToCart(cartId, addItemRequest.getProductId(), addItemRequest.getQuantity());
        return ResponseEntity.noContent().build();
    }


}

