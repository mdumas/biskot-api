package com.biskot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = "com.biskot")
@EnableCaching
@EntityScan("com.biskot.infra.repository.entity")
@EnableJpaRepositories({"com.biskot.infra.repository"})
@EnableTransactionManagement
public class BiskotApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiskotApiApplication.class, args);
	}

}
