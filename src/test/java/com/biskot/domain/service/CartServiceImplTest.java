package com.biskot.domain.service;

import com.biskot.domain.exception.CartDoesNotExistException;
import com.biskot.domain.exception.InvalidCartUpdateException;
import com.biskot.domain.exception.ProductDoesNotExistException;
import com.biskot.domain.model.*;
import com.biskot.domain.spi.CartPersistencePort;
import com.biskot.domain.spi.ProductPort;
import com.biskot.domain.validator.CartUpdateValidator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartServiceImplTest {

    @Mock
    private CartPersistencePort cartPersistencePort;

    @Mock
    private ProductPort productPort;

    @Mock
    private CartUpdateValidator cartUpdateValidator;

    @InjectMocks
    private CartServiceImpl cartService;

    @Test
    public void should_create_cart() {
        // Given
        Cart expectedCart = new Cart();
        when(cartPersistencePort.saveCart(any(Cart.class))).thenReturn(expectedCart);

        // When
        Cart cart = cartService.createCart();

        // Then
        assertThat(cart).isSameAs(expectedCart);
    }

    @Test
    public void should_not_get_cart_when_it_does_not_exist() {
        // Given
        when(cartPersistencePort.getCart(10L)).thenReturn(Optional.empty());

        // When
        Cart cart = cartService.getCart(10L);

        // Then
        assertThat(cart).isNull();
    }

    @Test
    public void should_get_cart() {
        // Given
        Cart expectedCart = new Cart();
        when(cartPersistencePort.getCart(10L)).thenReturn(Optional.of(expectedCart));

        // When
        Cart cart = cartService.getCart(10L);

        // Then
        assertThat(cart).isSameAs(expectedCart);
    }

    @Test
    public void should_not_add_item_to_cart_when_cart_does_not_exist() {
        // Given

        // When
        assertThrows(CartDoesNotExistException.class, () -> {
            cartService.addItemToCart(1L, 1L, 10);
        });

        // Then
    }

    @Test
    public void should_not_add_item_to_cart_when_product_does_not_exist() {
        // Given
        when(cartPersistencePort.getCart(1L)).thenReturn(Optional.of(new Cart()));

        // When
        assertThrows(ProductDoesNotExistException.class, () -> {
            cartService.addItemToCart(1L, 1L, 10);
        });

        // Then
    }

    @Test
    public void should_not_add_item_to_cart_when_validation_errors() {
        // Given
        Cart cart = new Cart();
        when(cartPersistencePort.getCart(1L)).thenReturn(Optional.of(cart));

        Product product = new Product();
        product.setId(1L);
        product.setLabel("label");
        product.setQuantityInStock(20);
        product.setUnitPrice(BigDecimal.TEN);

        when(productPort.getProduct(1L)).thenReturn(product);

        when(cartUpdateValidator.validate(cart, product, 10))
                .thenReturn(List.of(new ValidationError(ValidationErrorType.NOT_ENOUGH_QUANTITY)));

        // When
        assertThrows(InvalidCartUpdateException.class, () -> {
            cartService.addItemToCart(1L, 1L, 10);
        });

        // Then
    }

    @Test
    public void should_add_item_to_cart_when_new_item() {
        // Given
        Cart cart = new Cart();
        when(cartPersistencePort.getCart(1L)).thenReturn(Optional.of(cart));

        Product product = new Product();
        product.setId(1L);
        product.setLabel("label");
        product.setQuantityInStock(20);
        product.setUnitPrice(BigDecimal.TEN);

        when(productPort.getProduct(1L)).thenReturn(product);

        // When
        cartService.addItemToCart(1L, 1L, 10);

        // Then
        verify(cartUpdateValidator).validate(cart, product, 10);

        assertThat(cart.getItems()).hasSize(1);
        assertThat(cart.getItems().get(0).getProductId()).isEqualTo(1L);
        assertThat(cart.getItems().get(0).getProductLabel()).isEqualTo("label");
        assertThat(cart.getItems().get(0).getQuantity()).isEqualTo(10);
        assertThat(cart.getItems().get(0).getUnitPrice()).isEqualTo("10");
        assertThat(cart.getItems().get(0).getLinePrice()).isEqualTo("100");
    }

    @Test
    public void should_add_item_to_cart_when_existing_item() {
        // Given
        Item item = new Item();
        item.setProductId(1L);
        item.setProductLabel("label");
        item.setQuantity(5);
        item.setUnitPrice(BigDecimal.TEN);

        Cart cart = new Cart();
        cart.getItems().add(item);

        when(cartPersistencePort.getCart(1L)).thenReturn(Optional.of(cart));

        Product product = new Product();
        product.setId(1L);
        product.setLabel("label");
        product.setQuantityInStock(20);
        product.setUnitPrice(BigDecimal.TEN);

        when(productPort.getProduct(1L)).thenReturn(product);

        // When
        cartService.addItemToCart(1L, 1L, 10);

        // Then
        verify(cartUpdateValidator).validate(cart, product, 10);

        assertThat(cart.getItems()).hasSize(1);
        assertThat(cart.getItems().get(0).getProductId()).isEqualTo(1L);
        assertThat(cart.getItems().get(0).getProductLabel()).isEqualTo("label");
        assertThat(cart.getItems().get(0).getQuantity()).isEqualTo(10);
        assertThat(cart.getItems().get(0).getUnitPrice()).isEqualTo("10");
        assertThat(cart.getItems().get(0).getLinePrice()).isEqualTo("100");
    }

}