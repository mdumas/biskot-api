package com.biskot.domain.validator;

import com.biskot.domain.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartUpdateValidatorTest {

    @Mock
    private Environment environment;

    @InjectMocks
    private CartUpdateValidator validator;

    @BeforeEach
    void setUp() {
        when(environment.getRequiredProperty("business-rules.max-total-price-allowed", BigDecimal.class))
                .thenReturn(new BigDecimal("100"));

        when(environment.getRequiredProperty("business-rules.max-products-allowed", Integer.class))
                .thenReturn(3);
    }

    @Test
    public void should_validate_when_not_enough_quantity() {
        // Given
        Product product = new Product();
        product.setQuantityInStock(99);

        // When
        List<ValidationError> validationErrors = validator.validate(new Cart(), product, 100);

        // Then
        assertThat(validationErrors).hasSize(1);
        assertThat(validationErrors.get(0).getValidationErrorType()).isEqualTo(ValidationErrorType.NOT_ENOUGH_QUANTITY);
        assertThat(validationErrors.get(0).getParams()).containsOnly(99);
    }

    @Test
    public void should_validate_when_max_total_price_exceeded() {
        // Given
        Product product = new Product();
        product.setUnitPrice(new BigDecimal("20"));
        product.setQuantityInStock(99);

        Item item = new Item();
        item.setUnitPrice(new BigDecimal("20"));
        item.setQuantity(10);

        Cart cart = new Cart();
        cart.setItems(List.of(item));

        // When
        List<ValidationError> validationErrors = validator.validate(cart, product, 10);

        // Then
        assertThat(validationErrors).hasSize(1);
        assertThat(validationErrors.get(0).getValidationErrorType()).isEqualTo(ValidationErrorType.MAX_TOTAL_PRICE_EXCEEDED);
        assertThat(validationErrors.get(0).getParams()).containsOnly(new BigDecimal("100"));
    }

    @Test
    public void should_validate_when_max_product_exceeded() {
        // Given
        Product product = new Product();
        product.setUnitPrice(new BigDecimal("20"));
        product.setQuantityInStock(99);

        Item item1 = new Item();
        item1.setUnitPrice(new BigDecimal("20"));
        item1.setQuantity(1);

        Item item2 = new Item();
        item2.setUnitPrice(new BigDecimal("5"));
        item2.setQuantity(1);

        Item item3 = new Item();
        item3.setUnitPrice(new BigDecimal("2"));
        item3.setQuantity(2);

        Item item4 = new Item();
        item4.setUnitPrice(new BigDecimal("10"));
        item4.setQuantity(1);

        Cart cart = new Cart();
        cart.setItems(List.of(item1, item2, item3, item4));

        // When
        List<ValidationError> validationErrors = validator.validate(cart, product, 1);

        // Then
        assertThat(validationErrors).hasSize(1);
        assertThat(validationErrors.get(0).getValidationErrorType()).isEqualTo(ValidationErrorType.MAX_PRODUCTS_EXCEEDED);
        assertThat(validationErrors.get(0).getParams()).containsOnly(3);
    }


    @Test
    public void should_validate_without_error() {
        // Given
        Product product = new Product();
        product.setUnitPrice(new BigDecimal("20"));
        product.setQuantityInStock(99);

        Item item1 = new Item();
        item1.setUnitPrice(new BigDecimal("20"));
        item1.setQuantity(1);

        Item item2 = new Item();
        item2.setUnitPrice(new BigDecimal("5"));
        item2.setQuantity(1);

        Item item3 = new Item();
        item3.setUnitPrice(new BigDecimal("2"));
        item3.setQuantity(2);

        Cart cart = new Cart();
        cart.setItems(List.of(item1, item2, item3));

        // When
        List<ValidationError> validationErrors = validator.validate(cart, product, 1);

        // Then
        assertThat(validationErrors).isEmpty();
    }

}