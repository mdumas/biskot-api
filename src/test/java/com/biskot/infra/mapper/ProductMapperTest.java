package com.biskot.infra.mapper;

import com.biskot.domain.model.Product;
import com.biskot.infra.gateway.payload.ProductResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ProductMapperTest {

    private final ProductMapper mapper = Mappers.getMapper(ProductMapper.class);

    @Test
    public void should_map() {
        // Given
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(1L);
        productResponse.setLabel("label_1");
        productResponse.setQuantityInStock(10);
        productResponse.setUnitPrice(BigDecimal.TEN);

        // When
        Product product = mapper.map(productResponse);

        // Then
        assertThat(product.getId()).isEqualTo(1L);
        assertThat(product.getLabel()).isEqualTo("label_1");
        assertThat(product.getQuantityInStock()).isEqualTo(10);
        assertThat(product.getUnitPrice()).isEqualTo("10");
    }

}