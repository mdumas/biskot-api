package com.biskot.infra.mapper;

import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Item;
import com.biskot.infra.repository.entity.CartEntity;
import com.biskot.infra.repository.entity.ItemEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.extractProperty;

class CartMapperTest {

    private final CartMapper mapper = Mappers.getMapper(CartMapper.class);

    @Test
    public void should_map_cart() {
        // Given
        Cart cart = new Cart();
        cart.setId(1L);
        cart.setItems(createItems());

        // When
        CartEntity cartEntity = mapper.map(cart);

        // Then
        assertThat(cartEntity.getId()).isEqualTo(1L);

        List<ItemEntity> items = cartEntity.getItems();
        assertThat(extractProperty("productId").from(items)).containsExactly(1L, 2L);
        assertThat(extractProperty("productLabel").from(items)).containsExactly("label_1", "label_2");
        assertThat(extractProperty("quantity").from(items)).containsExactly(1, 2);
        assertThat(extractProperty("unitPrice").from(items)).containsExactly(new BigDecimal("10"), new BigDecimal("20"));
    }

    @Test
    public void should_map_cart_entity() {
        // Given
        CartEntity cartEntity = new CartEntity();
        cartEntity.setId(1L);
        cartEntity.setItems(createItemEntities());

        // When
        Cart cart = mapper.map(cartEntity);

        // Then
        assertThat(cart.getId()).isEqualTo(1L);

        List<Item> items = cart.getItems();
        assertThat(extractProperty("productId").from(items)).containsExactly(1L, 2L);
        assertThat(extractProperty("productLabel").from(items)).containsExactly("label_1", "label_2");
        assertThat(extractProperty("quantity").from(items)).containsExactly(1, 2);
        assertThat(extractProperty("unitPrice").from(items)).containsExactly(new BigDecimal("10"), new BigDecimal("20"));
        assertThat(extractProperty("linePrice").from(items)).containsExactly(new BigDecimal("10"), new BigDecimal("40"));

        assertThat(cart.getTotalPrice()).isEqualTo(new BigDecimal("50"));
    }

    private static List<Item> createItems() {
        List<Item> items = new ArrayList<>();

        for (int i = 1; i <= 2; ++i) {
            Item item = new Item();
            item.setProductId((long) i);
            item.setProductLabel("label_" + i);
            item.setQuantity(i);
            item.setUnitPrice(BigDecimal.TEN.multiply(BigDecimal.valueOf(i)));

            items.add(item);
        }

        return items;
    }

    private static List<ItemEntity> createItemEntities() {
        List<ItemEntity> items = new ArrayList<>();

        for (int i = 1; i <= 2; ++i) {
            ItemEntity item = new ItemEntity();
            item.setProductId((long) i);
            item.setProductLabel("label_" + i);
            item.setQuantity(i);
            item.setUnitPrice(BigDecimal.TEN.multiply(BigDecimal.valueOf(i)));

            items.add(item);
        }

        return items;
    }

}