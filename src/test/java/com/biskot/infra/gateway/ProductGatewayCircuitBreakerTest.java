package com.biskot.infra.gateway;

import com.biskot.app.BiskotApiApplication;
import com.biskot.domain.model.Product;
import com.biskot.infra.mock.ProductMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = BiskotApiApplication.class) // Use same configuration settings as others ITs to speed up the execution time.
@AutoConfigureMockMvc
class ProductGatewayCircuitBreakerTest {

    @Autowired
    private ProductMockServer productMockServer;

    @Autowired
    private CacheManager cacheManager;

    @Autowired
    private ProductGateway gateway;

    private List<StubMapping> stubMappings = new ArrayList<>();

    @AfterEach
    void tearDown() {
        cacheManager.getCache(ProductGateway.PRODUCT_CACHE_NAME).clear();
        stubMappings.forEach(stubMapping -> productMockServer.getWireMockServer().removeStubMapping(stubMapping));
    }

    @Test
    public void should_not_get_product_on_first_call_when_error() {
        // Given
        mockProductServer(500);

        // When
        assertThrows(IllegalStateException.class, () -> {
            gateway.getProduct(5L);
        });

        // Then
    }

    @Test
    public void should_get_product_from_cache_on_second_call_when_failure() throws Exception {
        // Given
        mockProductServer(200, Files.readString(Paths.get("src/main/resources/mocks/product_5.json")));

        // When
        Product product = gateway.getProduct(5L);

        // Then
        assertThat(product).isNotNull();

        // Given
        mockProductServer(500);

        // When
        Product productFromCache = gateway.getProduct(5L);

        // Then
        assertThat(productFromCache).isNotNull();
    }


    private void mockProductServer(int status) {
        mockProductServer(status, null);
    }

    private void mockProductServer(int status, String body) {
        ResponseDefinitionBuilder responseDefinitionBuilder = aResponse().withStatus(status);

        if (body != null) {
            responseDefinitionBuilder = responseDefinitionBuilder
                    .withHeader("Content-Type", "application/json")
                    .withBody(body);
        }

        StubMapping stubMapping = productMockServer.getWireMockServer().stubFor(get(urlPathEqualTo("/products/5"))
                .willReturn(responseDefinitionBuilder));

        stubMappings.add(stubMapping);
    }


}