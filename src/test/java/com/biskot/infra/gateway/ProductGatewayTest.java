package com.biskot.infra.gateway;

import com.biskot.domain.model.Product;
import com.biskot.infra.mapper.ProductMapper;
import com.biskot.infra.gateway.payload.ProductResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import static com.biskot.infra.gateway.ProductGateway.PRODUCT_CACHE_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ProductGatewayTest {

    private RestTemplate restTemplate;
    private ProductMapper productMapper;
    private ProductGateway gateway;
    private CacheManager cacheManager;

    @BeforeEach
    void setUp() {
        restTemplate = mock(RestTemplate.class);
        productMapper = mock(ProductMapper.class);
        cacheManager = mock(CacheManager.class);

        RestTemplateBuilder restTemplateBuilder = mock(RestTemplateBuilder.class, RETURNS_DEEP_STUBS);
        when(restTemplateBuilder.errorHandler(any(ResponseErrorHandler.class)).build()).thenReturn(restTemplate);

        gateway = new ProductGateway(restTemplateBuilder, "http://localhost:9001", productMapper, cacheManager);
    }

    @Test
    public void should_not_get_product_when_404() {
        // Given
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(restTemplate.getForEntity("http://localhost:9001/products/{id}", ProductResponse.class, 1L)).thenReturn(responseEntity);

        // When
        Product product = gateway.getProduct(1L);

        // Then
        assertThat(product).isNull();
    }

    @Test
    public void should_not_get_product_when_error() {
        // Given
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        when(restTemplate.getForEntity("http://localhost:9001/products/{id}", ProductResponse.class, 1L)).thenReturn(responseEntity);

        // When
        assertThrows(IllegalStateException.class, () -> {
            gateway.getProduct(1L);
        });

        // Then
    }

    @Test
    public void should_not_get_product_when_empty_body() {
        // Given
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(HttpStatus.OK);
        when(restTemplate.getForEntity("http://localhost:9001/products/{id}", ProductResponse.class, 1L)).thenReturn(responseEntity);

        // When
        assertThrows(IllegalStateException.class, () -> {
            gateway.getProduct(1L);
        });

        // Then
    }

    @Test
    public void should_get_product() {
        // Given
        Cache cache = mock(Cache.class);
        when(cacheManager.getCache(PRODUCT_CACHE_NAME)).thenReturn(cache);

        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(1L);
        ResponseEntity<ProductResponse> responseEntity = new ResponseEntity<>(productResponse, HttpStatus.OK);
        when(restTemplate.getForEntity("http://localhost:9001/products/{id}", ProductResponse.class, 1L)).thenReturn(responseEntity);

        Product expectedProduct = new Product();
        when(productMapper.map(productResponse)).thenReturn(expectedProduct);

        // When
        Product product = gateway.getProduct(1L);

        // Then
        verify(cache).put(1L, productResponse);
        assertThat(product).isSameAs(expectedProduct);
    }

}