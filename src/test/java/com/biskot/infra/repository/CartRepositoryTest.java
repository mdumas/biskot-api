package com.biskot.infra.repository;

import com.biskot.domain.model.Cart;
import com.biskot.infra.mapper.CartMapper;
import com.biskot.infra.repository.entity.CartEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartRepositoryTest {

    @Mock
    private JpaCartRepository jpaCartRepository;

    @Mock
    private CartMapper cartMapper;

    @InjectMocks
    private CartRepository repository;

    @Test
    public void should_save_cart() {
        // Given
        Cart inputCart = new Cart();
        CartEntity cartEntity = new CartEntity();
        when(cartMapper.map(inputCart)).thenReturn(cartEntity);

        when(jpaCartRepository.save(cartEntity)).thenReturn(cartEntity);

        Cart expectedCart = new Cart();
        when(cartMapper.map(cartEntity)).thenReturn(expectedCart);

        // When
        Cart cart = repository.saveCart(inputCart);

        // Then
        assertThat(cart).isSameAs(expectedCart);
    }

    @Test
    public void should_not_get_cart_when_it_does_not_exist() {
        // Given

        // When
        Optional<Cart> cart = repository.getCart(10L);

        // Then
        assertThat(cart).isNotPresent();
    }

    @Test
    public void should_get_cart() {
        // Given
        CartEntity cartEntity = new CartEntity();
        when(jpaCartRepository.findById(1L)).thenReturn(Optional.of(cartEntity));

        Cart expectedCart = new Cart();
        when(cartMapper.map(cartEntity)).thenReturn(expectedCart);

        // When
        Optional<Cart> optional = repository.getCart(1L);

        // Then
        assertThat(optional).isPresent();
        assertThat(optional.get()).isSameAs(expectedCart);
    }

}