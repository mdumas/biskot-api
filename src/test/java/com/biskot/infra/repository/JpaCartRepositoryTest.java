package com.biskot.infra.repository;

import com.biskot.app.BiskotApiApplication;
import com.biskot.infra.repository.entity.CartEntity;
import com.biskot.infra.repository.entity.ItemEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.extractProperty;

@DataJpaTest
@ContextConfiguration(classes = BiskotApiApplication.class)
class JpaCartRepositoryTest {

    @Autowired
    private JpaCartRepository repository;

    @Test
    public void should_save_and_find_by_id() {
        // Given
        ItemEntity itemEntity1 = new ItemEntity();
        itemEntity1.setProductId(1L);
        itemEntity1.setProductLabel("product_label_1");
        itemEntity1.setQuantity(10);
        itemEntity1.setUnitPrice(BigDecimal.TEN);

        ItemEntity itemEntity2 = new ItemEntity();
        itemEntity2.setProductId(2L);
        itemEntity2.setProductLabel("product_label_2");
        itemEntity2.setQuantity(15);
        itemEntity2.setUnitPrice(new BigDecimal("25"));

        CartEntity cartEntity = new CartEntity();
        cartEntity.setItems(List.of(itemEntity1, itemEntity2));

        repository.save(cartEntity);

        // When
        Optional<CartEntity> optional = repository.findById(cartEntity.getId());

        // Then
        assertThat(optional).isPresent();

        CartEntity actualCartEntity = optional.get();
        assertThat(actualCartEntity.getId()).isNotNull();

        List<ItemEntity> items = actualCartEntity.getItems();
        assertThat(items).hasSize(2);
        assertThat(extractProperty("productId").from(items)).containsExactly(1L, 2L);
        assertThat(extractProperty("productLabel").from(items)).containsExactly("product_label_1", "product_label_2");
        assertThat(extractProperty("quantity").from(items)).containsExactly(10, 15);
        assertThat(extractProperty("unitPrice").from(items)).containsExactly(BigDecimal.TEN, new BigDecimal("25"));
    }

}