package com.biskot.app.mapper;

import com.biskot.app.contract.model.CartResponse;
import com.biskot.app.contract.model.ItemResponse;
import com.biskot.domain.model.Cart;
import com.biskot.domain.model.Item;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.extractProperty;

class CartResponseMapperTest {

    private final CartResponseMapper mapper = Mappers.getMapper(CartResponseMapper.class);

    @Test
    public void should_map() {
        // Given
        Cart cart = new Cart();
        cart.setId(1L);
        cart.setItems(createItems());

        // When
        CartResponse cartResponse = mapper.map(cart);

        // Then
        assertThat(cartResponse.getId()).isEqualTo(1L);

        List<ItemResponse> items = cartResponse.getItems();
        assertThat(extractProperty("productId").from(items)).containsExactly(1L, 2L);
        assertThat(extractProperty("productLabel").from(items)).containsExactly("label_1", "label_2");
        assertThat(extractProperty("quantity").from(items)).containsExactly(1, 2);
        assertThat(extractProperty("unitPrice").from(items)).containsExactly(BigDecimal.TEN, new BigDecimal("20"));
        assertThat(extractProperty("linePrice").from(items)).containsExactly(BigDecimal.TEN, new BigDecimal("40"));

        assertThat(cartResponse.getTotalPrice()).isEqualTo(new BigDecimal("50"));
    }

    private static List<Item> createItems() {
        List<Item> items = new ArrayList<>();

        for (int i = 1; i <= 2; ++i) {
            Item item = new Item();
            item.setProductId((long) i);
            item.setProductLabel("label_" + i);
            item.setQuantity(i);
            item.setUnitPrice(BigDecimal.TEN.multiply(BigDecimal.valueOf(i)));

            items.add(item);
        }

        return items;
    }

}