package com.biskot.app.rest;

import com.biskot.app.BiskotApiApplication;
import com.biskot.app.contract.model.AddItemRequest;
import com.biskot.app.contract.model.CartResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(classes = BiskotApiApplication.class)
@AutoConfigureMockMvc
class CartControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void should_create_cart() throws Exception {
        // Given

        // When
        ResultActions actions = mockMvc.perform(post("/carts").accept(MediaType.APPLICATION_JSON));

        // Then
        String json = actions.andReturn().getResponse().getContentAsString();
        CartResponse cartResponse = mapper.readValue(json, CartResponse.class);

        actions.andExpect(status().isCreated())
               .andExpect(header().string("location", String.format("http://localhost/carts/%d", cartResponse.getId())))
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.id").value(cartResponse.getId()))
               .andExpect(jsonPath("$.items").value(empty()))
               .andExpect(jsonPath("$.total_price").value(new BigDecimal("0")));
    }

    @Test
    public void should_not_get_cart_when_not_existing() throws Exception {
        // Given

        // When
        ResultActions actions = mockMvc.perform(get("/carts/{id}", Long.MAX_VALUE).accept(MediaType.APPLICATION_JSON));

        // Then
        actions.andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("404"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("The requested cart does not exist"))
               .andExpect(jsonPath("$.errors").doesNotExist());
    }

    @Test
    public void should_get_cart() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // When
        ResultActions actions = mockMvc.perform(get("/carts/{id}", cartResponse.getId()).accept(MediaType.APPLICATION_JSON));

        // Then
        actions.andExpect(status().isOk())
               .andExpect(content().contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$.id").value(cartResponse.getId()))
               .andExpect(jsonPath("$.items").value(empty()))
               .andExpect(jsonPath("$.total_price").value(new BigDecimal("0")));
    }

    @Test
    public void should_not_add_item_to_cart_when_no_product_id() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, null, 10);

        // Then
        actions.andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("400"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("Bad request format"))
               .andExpect(jsonPath("$.errors").value(hasSize(1)))
               .andExpect(jsonPath("$.errors[0].code").doesNotExist())
               .andExpect(jsonPath("$.errors[0].field").value("productId"))
               .andExpect(jsonPath("$.errors[0].message").value("productId must not be null"));
    }

    @Test
    public void should_not_add_item_to_cart_when_no_quantity() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, 1L, null);

        // Then
        actions.andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("400"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("Bad request format"))
               .andExpect(jsonPath("$.errors").value(hasSize(1)))
               .andExpect(jsonPath("$.errors[0].code").doesNotExist())
               .andExpect(jsonPath("$.errors[0].field").value("quantity"))
               .andExpect(jsonPath("$.errors[0].message").value("quantity must not be null"));
    }

    @Test
    public void should_not_add_item_to_cart_when_negative_product_id() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, -10L, 5);

        // Then
        actions.andExpect(status().isBadRequest())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("400"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("Bad request format"))
               .andExpect(jsonPath("$.errors").value(hasSize(1)))
               .andExpect(jsonPath("$.errors[0].code").doesNotExist())
               .andExpect(jsonPath("$.errors[0].field").value("productId"))
               .andExpect(jsonPath("$.errors[0].message").value("productId must be greater than or equal to 1"));
    }

    @Test
    public void should_not_add_item_to_cart_when_negative_quantity() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, 1L, -5);

        // Then
        actions.andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(jsonPath("$.status").value("400"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.message").value("Bad request format"))
                .andExpect(jsonPath("$.errors").value(hasSize(1)))
                .andExpect(jsonPath("$.errors[0].code").doesNotExist())
                .andExpect(jsonPath("$.errors[0].field").value("quantity"))
                .andExpect(jsonPath("$.errors[0].message").value("quantity must be greater than or equal to 1"));
    }

    @Test
    public void should_not_add_item_to_cart_when_zero_product_id() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, 0L, 5);

        // Then
        actions.andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(jsonPath("$.status").value("400"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.message").value("Bad request format"))
                .andExpect(jsonPath("$.errors").value(hasSize(1)))
                .andExpect(jsonPath("$.errors[0].code").doesNotExist())
                .andExpect(jsonPath("$.errors[0].field").value("productId"))
                .andExpect(jsonPath("$.errors[0].message").value("productId must be greater than or equal to 1"));
    }

    @Test
    public void should_not_add_item_to_cart_when_zero_quantity() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, 1L, 0);

        // Then
        actions.andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                .andExpect(jsonPath("$.status").value("400"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.message").value("Bad request format"))
                .andExpect(jsonPath("$.errors").value(hasSize(1)))
                .andExpect(jsonPath("$.errors[0].code").doesNotExist())
                .andExpect(jsonPath("$.errors[0].field").value("quantity"))
                .andExpect(jsonPath("$.errors[0].message").value("quantity must be greater than or equal to 1"));
    }

    @Test
    public void should_not_add_item_to_cart_when_non_existing_cart() throws Exception {
        // Given

        // When
        ResultActions actions = addItem(Long.MAX_VALUE, 1L, 10);

        // Then
        // Then
        actions.andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("404"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("The requested cart does not exist"))
               .andExpect(jsonPath("$.errors").doesNotExist());
    }

    @Test
    public void should_not_add_item_to_cart_when_non_existing_product() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // When
        ResultActions actions = addItem(cartResponse.getId(), Long.MAX_VALUE, 10);

        // Then
        actions.andExpect(status().isNotFound())
               .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
               .andExpect(jsonPath("$.status").value("404"))
               .andExpect(jsonPath("$.timestamp").exists())
               .andExpect(jsonPath("$.message").value("The requested product does not exist"))
               .andExpect(jsonPath("$.errors").doesNotExist());
    }

    @Test
    public void should_not_add_item_to_cart_when_quantity_exceed_stock_availability() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // When
        ResultActions updateCartResult = addItem(cartResponse.getId(), 1L, 500);

        // Then
        updateCartResult.andExpect(status().isBadRequest())
                        .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                        .andExpect(jsonPath("$.status").value("400"))
                        .andExpect(jsonPath("$.timestamp").exists())
                        .andExpect(jsonPath("$.message").value("Business rules validation failed"))
                        .andExpect(jsonPath("$.errors").value(hasSize(2)))
                        .andExpect(jsonPath("$.errors[0].code").value("NOT_ENOUGH_QUANTITY"))
                        .andExpect(jsonPath("$.errors[0].field").doesNotExist())
                        .andExpect(jsonPath("$.errors[0].message").value("Added quantity of a product should not exceed the stock availability (200)"))
                        .andExpect(jsonPath("$.errors[1].code").value("MAX_TOTAL_PRICE_EXCEEDED"))
                        .andExpect(jsonPath("$.errors[1].field").doesNotExist())
                        .andExpect(jsonPath("$.errors[1].message").value("Total price of the cart should not exceed 100 euros"));
    }

    @Test
    public void should_not_add_item_to_cart_when_total_price_exceed_100_euros() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // When
        ResultActions updateCartResult = addItem(cartResponse.getId(), 4L, 2);

        // Then
        updateCartResult.andExpect(status().isBadRequest())
                        .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                        .andExpect(jsonPath("$.status").value("400"))
                        .andExpect(jsonPath("$.timestamp").exists())
                        .andExpect(jsonPath("$.message").value("Business rules validation failed"))
                        .andExpect(jsonPath("$.errors").value(hasSize(1)))
                        .andExpect(jsonPath("$.errors[0].code").value("MAX_TOTAL_PRICE_EXCEEDED"))
                        .andExpect(jsonPath("$.errors[0].field").doesNotExist())
                        .andExpect(jsonPath("$.errors[0].message").value("Total price of the cart should not exceed 100 euros"));
    }

    @Test
    public void should_not_add_item_to_cart_when_more_than_3_different_products() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        addItem(cartResponse.getId(), 1L, 1);
        addItem(cartResponse.getId(), 2L, 1);
        addItem(cartResponse.getId(), 3L, 1);

        // When
        ResultActions updateCartResult = addItem(cartResponse.getId(), 4L, 1);

        // Then
        updateCartResult.andExpect(status().isBadRequest())
                        .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
                        .andExpect(jsonPath("$.status").value("400"))
                        .andExpect(jsonPath("$.timestamp").exists())
                        .andExpect(jsonPath("$.message").value("Business rules validation failed"))
                        .andExpect(jsonPath("$.errors").value(hasSize(2)))
                        .andExpect(jsonPath("$.errors[0].code").value("MAX_TOTAL_PRICE_EXCEEDED"))
                        .andExpect(jsonPath("$.errors[0].field").doesNotExist())
                        .andExpect(jsonPath("$.errors[0].message").value("Total price of the cart should not exceed 100 euros"))
                        .andExpect(jsonPath("$.errors[1].code").value("MAX_PRODUCTS_EXCEEDED"))
                        .andExpect(jsonPath("$.errors[1].field").doesNotExist())
                        .andExpect(jsonPath("$.errors[1].message").value("A cart cannot contain more than 3 different products"));
    }

    @Test
    public void should_add_item_to_cart_when_new_item() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // When
        ResultActions updateCartResult = addItem(cartResponse.getId(), 1L, 10);
        ResultActions getCartResult = mockMvc.perform(get("/carts/{id}", cartResponse.getId()));

        // Then
        updateCartResult.andExpect(status().isNoContent());

        getCartResult.andExpect(status().isOk())
                     .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                     .andExpect(jsonPath("$.id").value(cartResponse.getId()))
                     .andExpect(jsonPath("$.items").value(hasSize(1)))
                     .andExpect(jsonPath("$.items[0].product_id").value(1L))
                     .andExpect(jsonPath("$.items[0].product_label").value("Déodorant Spray 200ml Ice Dive ADIDAS"))
                     .andExpect(jsonPath("$.items[0].quantity").value(10))
                     .andExpect(jsonPath("$.items[0].unit_price").value(2.0d))
                     .andExpect(jsonPath("$.items[0].line_price").value(20.0d))
                     .andExpect(jsonPath("$.total_price").value(new BigDecimal("20.0")));
    }

    @Test
    public void should_add_item_to_cart_when_existing_item() throws Exception {
        // Given
        CartResponse cartResponse = createCart();

        // Add first item.
        addItem(cartResponse.getId(), 1L, 10);

        // When
        ResultActions updateCartResult = addItem(cartResponse.getId(), 1L, 15);
        ResultActions getCartResult = mockMvc.perform(get("/carts/{id}", cartResponse.getId()));

        // Then
        updateCartResult.andExpect(status().isNoContent());

        getCartResult.andExpect(status().isOk())
                     .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                     .andExpect(jsonPath("$.id").value(cartResponse.getId()))
                     .andExpect(jsonPath("$.items").value(hasSize(1)))
                     .andExpect(jsonPath("$.items[0].product_id").value(1L))
                     .andExpect(jsonPath("$.items[0].product_label").value("Déodorant Spray 200ml Ice Dive ADIDAS"))
                     .andExpect(jsonPath("$.items[0].quantity").value(15))
                     .andExpect(jsonPath("$.items[0].unit_price").value(2.0d))
                     .andExpect(jsonPath("$.items[0].line_price").value(30.0d))
                     .andExpect(jsonPath("$.total_price").value(new BigDecimal("30.0")));
    }


    private CartResponse createCart() throws Exception {
        String jsonCart = mockMvc.perform(post("/carts").accept(MediaType.APPLICATION_JSON)).andReturn()
                .getResponse().getContentAsString();

        return mapper.readValue(jsonCart, CartResponse.class);
    }

    private ResultActions addItem(Long cartId, Long productId, Integer quantity) throws Exception {
        AddItemRequest request = new AddItemRequest();
        request.setProductId(productId);
        request.setQuantity(quantity);

        String json = mapper.writeValueAsString(request);

        MockHttpServletRequestBuilder requestBuilder = put("/carts/{id}/items", cartId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .accept(MediaType.APPLICATION_JSON);

        return mockMvc.perform(requestBuilder);
    }


}